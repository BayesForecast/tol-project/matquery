//////////////////////////////////////////////////////////////////////////////
//FILE: CsvTools.tol
//PURPOSE: Class @CsvTools for importing CVS/TXT files
//Author: Víctor de Buen Remiro (vdebuen@bayesforecast.com)
//CopyRight: Bayes Forecast (http://www.bayesforecast.com/)
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Class @CsvTools  
//////////////////////////////////////////////////////////////////////////////
{
  Text path = "";
  Real hasHeader = True;
  Text delimiter.register = "\n";
  Text delimiter.field = "\t";
  Text delimiter.text = "\"";
  Text delimiter.float = ".";
  Real examinedLines = 0;
  Real importedLines = 0;
  
  Set masks = [[
    [["9999-12-31", "2300-12-31"]]
  ]];
  
  Set dateFormats = [[
    "%cy%Ym%md%dh%hi%is%s",
    "%cy%Ym%md%d",
    "%cy%Ym%md",
    "%c%Y-%m",
    "%c%Y-%m-%d",
    "%c%Y-%m-%d %h:%i",
    "%c%Y-%m-%d %h:%i:%s",
    "%c%m-%Y",
    "%c%d-%m-%Y",
    "%c%d-%m-%Y %h:%i",
    "%c%d-%m-%Y %h:%i:%s",
    "%c%y-%m",
    "%c%y-%m-%d",
    "%c%y-%m-%d %h:%i",
    "%c%y-%m-%d %h:%i:%s",
    "%c%m-%y",
    "%c%d-%m-%y",
    "%c%d-%m-%y %h:%i",
    "%c%d-%m-%y %h:%i:%s",
    "%c%Y/%m",
    "%c%Y/%m/%d",
    "%c%Y/%m/%d %h:%i",
    "%c%Y/%m/%d %h:%i:%s",
    "%c%m/%Y",
    "%c%d/%m/%Y",
    "%c%d/%m/%Y %h:%i",
    "%c%d/%m/%Y %h:%i:%s",
    "%c%y/%m",
    "%c%y/%m/%d",
    "%c%y/%m/%d %h:%i",
    "%c%y/%m/%d %h:%i:%s",
    "%c%m/%y",
    "%c%d/%m/%y",
    "%c%d/%m/%y %h:%i",
    "%c%d/%m/%y %h:%i:%s"
  ]];
  
  Set userFieldNames = Copy(Empty);
  
  Set fieldNames = Copy(Empty);
  Set fieldExam = Copy(Empty);
  Set structures = Copy(Empty);
  Set fieldDef = Copy(Empty);
  Set fieldActive = Copy(Empty);
  Set fieldValues = Copy(Empty);
  Set fieldStats = Copy(Empty);
  Real posibleCombinations = ?;
  Text pathPref = "";
  
  ////////////////////////////////////////////////////////////////////////////
  Real ExamRows(Real examineRows) 
  ////////////////////////////////////////////////////////////////////////////
  {
    Text pathPref := GetFilePath(path)+GetFilePrefix(path);
    Real fh = FOpen(path,"r");
    Real ok = If(!fh,
    {
      WriteLn("[CsvTools] Cannot open for read file "<<path);
      False
    },
    {
      WriteLn("[CsvTools] Examinig text file "<<path+" ...");
      Set fieldNames := If(!hasHeader, userFieldNames, {
        Text header =  FGetText(fh, 1E6, delimiter.register);
        Set Tokenizer(header,delimiter.field)
      });
      Real n = Card(fieldNames);
      Set For(1,n,Real(Real col)
      {
        Text cell = fieldNames[col]; 
        Real startWithQuote = TextBeginWith(cell,delimiter.text);
        Real endsAtQuote = TextEndAt(cell,delimiter.text);
        Text clean = Compact(If(!(startWithQuote & endsAtQuote), cell,
        {
          Sub(cell,2,TextLength(cell)-1)  
        }));
        Text fieldNames[col] := Case(
          clean=="","Field_"<<col,
          1==1,ToName(clean));
        True
      });
      Set fieldExam := For(1,n,Set(Real col) { Eval(fieldNames[col]+"=Copy(Empty)") });
    //Set For(1, examineRows, Set(Real row_)
      Real examinedLines := 0;
      While(And(!FEof(fh),examinedLines<examineRows),
      {
        Real examinedLines := examinedLines+1;
        Real row = examinedLines;
        Set register = Tokenizer(FGetText(fh, 1E6, delimiter.register),delimiter.field);
        If(!(row%100), WriteLn("[CsvTools] Processing register "<<row));
        Real m = Card(register);
        If(m!=n,
        {
          WriteLn("[CsvTools] Register "<<row+" has "<<m+" instead of "<<n+" fields","E");
          Empty
        },
        For(1,n,Real(Real col)
        {
          Text field = fieldNames[col];
          Text cellOrig = register[col];
          Text cell = ReplaceTable(cellOrig,masks);
          Text c1 = Sub(cell,1,1);
          Text c2 = Sub(cell,2,2);
          Real seemsDate = Or(TextFind(cell,"-"),TextFind(cell,"/"),
                              And(c1=="y",TxtIsDigit(c2)));
          Real value = TextToReal(Replace(cell,delimiter.float,"."));
          Set fieldFormat = If(!IsUnknown(value), 
          {
            [[ [[field, row, Text type="Real", Text fmt="%lf", cell, value]] ]]
          }, 
          {
            Set dateOpt = SetConcat(EvalSet(dateFormats, Set(Text fmt_)
            {
              Text fmt = fmt_;
              Date value = TextToDate(cell, fmt);
              Text cell_ = FormatDate(value, fmt);
              If(cell!=cell_, Empty, 
                [[ [[field, row, Text type="Date", fmt, cell, value]] ]])
            }));
            Set dateCls = Classify(dateOpt,Real(Set a, Set b) { Compare(a::value, a::value) });
            Set dateFmts = EvalSet(dateCls,Set(Set c) { c[1] });
            If(Card(dateFmts)>=1, dateFmts,
            {
              Real startWithQuote = c1==delimiter.text;
              Real endsAtQuote = TextEndAt(cell,delimiter.text);
              Text value = Case(
                startWithQuote &  endsAtQuote, Sub(cell,2,TextLength(cell)-1),
                startWithQuote & !endsAtQuote, Sub(cell,2,TextLength(cell)  ),
               !startWithQuote &  endsAtQuote, Sub(cell,1,TextLength(cell)-1),
               !startWithQuote & !endsAtQuote, cell);
              [[ [[field, row, Text type="Text", Text fmt="%s", cell, value]] ]]
            })
          });
          Set Append(fieldExam[col],fieldFormat);
          True
        }))
      });
      Real FClose(fh);
      True
    });
    
    Set structures := EvalSet(fieldExam,Set(Set fe)
    {
      Set class = Classify(fe, Real(Set a, Set b)
      {
        Compare(a::type,b::type)*10+
        Compare(a::fmt, b::fmt)
      });
      Set fe_ = EvalSet(class,Set(Set c) { c[1] });
      Set fe__ = If(Card(fe_)<=1,fe_,
      {
        Select(fe_,Real(Set s)
        {
          !And(s::type=="Text",s::value=="")
        })
      });
      Eval(Name(fe)+"=fe__")
    });
    
    Real posibleCombinations := SetProd(EvalSet(structures,Card));
    
    Set fieldDef := If(posibleCombinations==1,
    {
      Set aux = EvalSet(SetConcat(structures),Set(Set fd) { Eval(fd::field+"=fd") });
      Real SetIndexByName(aux);
      aux
    });
    Real If(posibleCombinations==1,
    {
      Real OSDirMake(pathPref);
      Real Ois.Store(fieldDef,pathPref+"/fieldDef.oza")
    });
    posibleCombinations
  };
  
  
  ////////////////////////////////////////////////////////////////////////////
  Real Import(Real void)
  ////////////////////////////////////////////////////////////////////////////
  {
  //WriteLn("TRACE [MatQuery::@CsvTools::Import] 1");  
    Text pathPref := GetFilePath(path)+GetFilePrefix(path);
    Set fieldDef := { Include(pathPref+"/fieldDef.oza") };
    Real n = Card(fieldDef);
    Matrix cache = Constant(0,n,?);
    
    Real OSDirMake(pathPref);
  //WriteLn("TRACE [MatQuery::@CsvTools::Import] 2");  
    Set bbmPath = For(1,n,Text(Real col) 
    { 
      Text bbm = pathPref+"/"+(fieldDef[col])::field+".bbm";
      Matrix MatWriteFile(bbm,Constant(0,1,?));
      bbm
    });
  //WriteLn("TRACE [MatQuery::@CsvTools::Import] 3");  
    Set fieldActive := For(1,n,Real(Real col) 
    { 
      Real active = col>=1;
      Eval((fieldDef[col])::field+"=active") 
    });
  //WriteLn("TRACE [MatQuery::@CsvTools::Import] 4");  
    Set fieldValues := For(1,n,Set(Real col) { Eval((fieldDef[col])::field+"=Copy(Empty)") });
  //WriteLn("TRACE [MatQuery::@CsvTools::Import] 5");  
    Real _read.Disabled(Real col, Text cell)
    {
      0
    };
    
    Real _read.Real(Real col, Text cell)
    {
      TextToReal(cell)
    };
    Real _read.Date(Real col, Text cell)
    {
      DateToIndex(TextToDate(cell,(fieldDef[col])::fmt))
    };
    Real _read.Text(Real col, Text cell)
    {
      Text key = ToName(cell);
      Real found = FindIndexByName(fieldValues[col],key);
      If(found, found,
      {
        Real c = Card(fieldValues[col])+1;
      //WriteLn("\n[CsvTools] Adding to "+(fieldDef[col])::field+" "<<c+
      //  "-th key "+key+" found at register "<<row);
        Set Append(fieldValues[col], [[Eval(key+"=cell")]], True);
        c
      })
    };
  //WriteLn("TRACE [MatQuery::@CsvTools::Import] 6");  
    Set reader = For(1,n,Code(Real col)
    {
      Text type = (fieldDef[col])::type;
      Real active = fieldActive[col];
      Case(
      !active,      _read.Disabled, 
      type=="Real", _read.Real,
      type=="Text", _read.Text,
      type=="Date", _read.Date)
    });
  //WriteLn("TRACE [MatQuery::@CsvTools::Import] 7");  
    Real UpdateCache(Matrix rowData, Real force)
    {
      Real If(Rows(rowData), 
      {
        AppendRows(cache, rowData)
      });
      If(And(!force, Rows(cache)<1000), True,
      {
        Write(".");
      //WriteLn("TRACE [MatQuery::@CsvTools::Import] 7.1");  
//      Real Show(False,"ALL");
        Set For(1,n,Real(Real col)
        {
          Matrix MatAppendFile(bbmPath[col],SubCol(cache,[[col]]));
          True
        });
//      Real Show(True,"ALL");
        Matrix cache := Constant(0,n,?);
        True 
      })  
    };
  //WriteLn("TRACE [MatQuery::@CsvTools::Import] 8");  
    Real fh = FOpen(path,"r");
  //WriteLn("TRACE [MatQuery::@CsvTools::Import] 9");  
    Real ok = If(fh,True,
    {
      WriteLn("[ImportText] Cannot open for read file "<<path);
      False
    });
  //WriteLn("TRACE [MatQuery::@CsvTools::Import] 10");  
    Text header =  If(!ok,"",FGetText(fh, 1E6, delimiter.register));
    Real row = 0;
    While(And(!FEof(fh),row<1/0), {
      Real row := row+1;
      If(!(row%1000), WriteLn(""));
      Set register = Tokenizer(FGetText(fh, 1E6, delimiter.register),delimiter.field);
      Real m = Card(register);
      If(Card(register)!=n,
      {
        If(FEof(fh),True,
        { 
          WriteLn("[ImportText] Register "<<row+" has "<<m+" instead of "<<n+" fields","E");
          Real ok := False
        })    
      },
      { 
        Matrix rowData = SetRow(For(1,n,Real(Real col)
        {
          Code r = reader[col];
          r(col, register[col])
        })); 
        Real UpdateCache(rowData,False);
        True
      })
    });
    Real importedLines := row;
    WriteLn("");
    Real UpdateCache(Constant(0,n,?),True);
    
    Real FClose(fh);
  //WriteLn("TRACE [MatQuery::@CsvTools::Import] 10");  
    Set fieldStats := For(1,n,Set(Real col)
    {
      Text type = (fieldDef[col])::type;
      Matrix x = MatReadFile(bbmPath[col]);
      Matrix v = Sort(x, [[1]], True);
    
      Set { If(And(type=="Real",Rows(v)<=Min(20,Sqrt(Rows(x)))),
      {
        Matrix w = SubRow(x,MatSet(Tra(v))[1]);
        fieldValues[col] := MatSet(Tra(w))[1]
      }) };
      Set aux = [[
        Text (fieldDef[col])::field;
        Text (fieldDef[col])::type;
        Real continuous = MatSum(Abs(x-Round(x)))!=0;
        Real distinctValues = Rows(v);
        Real avr = MatAvr(x);
        Real stds = MatStDs(x);
        Real min = MatMin(x);
        Real max = MatMax(x)
      ]];
      { Eval((fieldDef[col])::field+"=aux") }
    });
  //WriteLn("TRACE [MatQuery::@CsvTools::Import] 11");  
    Real Ois.Store(fieldValues,pathPref+"/fieldValues.oza");
    Real Ois.Store(fieldStats,pathPref+"/fieldStats.oza");
    True
  }
};





/* */
